package com.queryme.androidlib;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public abstract class BridgeServerService extends Service {

	public BridgeServerService(String name) {
		super();
		// TODO Auto-generated constructor stub
	}

	// public abstract Class<? extends DBInfo> getDBInfo();
	public abstract int getPort();

	public abstract String getDatabaseName();

	public abstract int getDatabaseVersion();

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		connection();
		return 0;
	
	};

	public void connection() {
		try {
			QueryMeServer connector = QueryMeServer
					.getInstance(getApplicationContext());
			if (!connector.isConnected()) {
				connector.connect(getPort(), getDatabaseName(),
						getDatabaseVersion());
			}else{
				System.out.println("Reutilizando servidor conectado");
			}
			System.out.println("Servidor en espera de conexiones.");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public IBinder onBind(Intent arg0) {
		
		
		return null;
	}

	// @Override
	// protected void onStart() {
	// super.onStart();
	//
	//
	// }

	// @Override
	// protected void onNewIntent(Intent intent) {
	// // TODO Auto-generated method stub
	// super.onNewIntent(intent);
	// bridge.getRequest(intent);
	// }

	// @Override
	// protected void onHandleIntent(Intent intent) {
	// System.out.println("handler");
	// bridge = new Bridge(getApplicationContext());
	// bridge.setServerHelper(getHelper());
	// bridge.getRequest(intent);
	// }
}
