package com.queryme.androidlib;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLHelper extends SQLiteOpenHelper {

	protected String dbName;
	protected int dbVersion;
	private static SQLHelper instance;

	public static void clearInstance() {
//		if (instance != null) {
//			instance.close();
//			instance = null;
//		}
	}

	public static SQLHelper getInstance(Context context, String name,
			int version) {
		if (instance == null) {
			instance = new SQLHelper(context, name, version);
		} else {
			if (!instance.isDBInfo(name, version)) {
				instance.close();
				instance = new SQLHelper(context, name, version);
			}
		}

		return instance;
	}

	private boolean isDBInfo(String name, int version) {
		// TODO Auto-generated method stub
		return this.dbName.equals(name) && this.dbVersion == version;
	}

	public SQLHelper(Context context, String name, CursorFactory factory,
			int version, DatabaseErrorHandler errorHandler) {
		super(context, name, factory, version, errorHandler);
		init(name, version);
	}

	public SQLHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
		init(name, version);
	}

	public SQLHelper(Context context, String name, int version) {
		super(context, name, null, version);
		init(name, version);
	}

	private void init(String name, int version) {
		this.dbName = name;
		this.dbVersion = version;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

}
