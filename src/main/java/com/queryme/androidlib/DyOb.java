package com.queryme.androidlib;

import android.content.Context;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.Serializable;
import java.util.*;

public class DyOb {
	public static class Command {
		public String method;
		public String name;
		public int mode;
		public Object params;

		// public <P> P getParams(){
		// Type type = new TypeToken<P>() {
		// }.getType();
		// return gson.fromJson(params, type);
		// }

	}

	@SuppressWarnings("unchecked")
	public static Object execute(Context context, String jsonCommand) {
		Gson gson = new GsonBuilder().serializeNulls().create();

		Command command = gson.fromJson(jsonCommand, Command.class);
		Object result = null;
		Set<String> keys;
		
		if (command.method.equals("getSharedPreferencesKeys")) {
			keys = SharedPreferencesInspector.getSharedPreferencesKeys(context,
					command.name, command.mode);
			result = keys;
		} else if (command.method.equals("getSharedPreferences")) {
			List<String> ks = (List<String>) command.params;
			Map<String, Serializable> values = new TreeMap<String, Serializable>();
			for (String k : ks) {
				Serializable s = SharedPreferencesInspector
						.getSharedPreferences(context, command.name,
								command.mode, k);
				values.put(k, s);
			}
			result = values;
			//

		} else if (command.method.equals("setSharedPreferences")) {
			Map<String, Serializable> values = (Map<String, Serializable>) command.params;
			Iterator<String> ks = values.keySet().iterator();
			while (ks.hasNext()) {
				String k = ks.next();
				Serializable value = values.get(k);
				SharedPreferencesInspector.setSharedPreferences(context,
						command.name, command.mode, k, value);
			}

		}

		return result;
	}
}
