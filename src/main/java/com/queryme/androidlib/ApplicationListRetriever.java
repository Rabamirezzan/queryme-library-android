package com.queryme.androidlib;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.queryme.lib.models.QMApplication;
import com.queryme.lib.models.QMDatabase;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by rramirezb on 09/01/2015.
 */
public class ApplicationListRetriever {
    public static final String DATABASES_PATH = "/data/data/%s/databases";

    public static Context initForeignContext(Context context, String appPackage)
            throws PackageManager.NameNotFoundException {
        Context createdContext = null;

        createdContext = context.getApplicationContext().createPackageContext(
                appPackage,
                Context.CONTEXT_INCLUDE_CODE | Context.CONTEXT_IGNORE_SECURITY);

        return createdContext;
    }

    public static Context getContext(Context context, String appPackage) throws PackageManager.NameNotFoundException {
        Context fcontext = null;

        if (appPackage != null) {
            fcontext = initForeignContext(context, appPackage);
        }
        return fcontext;
    }

    public static PackageInfo getPackageInfo(Context context, String app) {
        PackageInfo pInfo = null;
        try {
            Context fcontext = getContext(context, app);

            if (fcontext != null) {
                pInfo = fcontext.getPackageManager().getPackageInfo(
                        fcontext.getPackageName(), 0);
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pInfo;
    }

    public static String getPackageVersionName(PackageInfo pInfo) {


        String version = "v [?]";

        if (pInfo != null) {
            String vn = (pInfo.versionName != null) ? pInfo.versionName
                    : "unknown";

            version = vn;
        }
        return version;
    }

    public static int getPackageVersion(PackageInfo pInfo) {


        int version = -1;

        if (pInfo != null) {
            version = pInfo.versionCode;
        }
        return version;
    }

    public static boolean hasDatabase(String app) {
        return getDatabasesFiles(app).length > 0;
    }

    public static List<QMApplication> getApplications(Context context) {
        List<ApplicationInfo> apps = context.getPackageManager().getInstalledApplications(PackageManager.GET_META_DATA);
        List<QMApplication> result = new ArrayList<QMApplication>();
        for (ApplicationInfo info : apps) {


            if (hasDatabase(info.packageName)) {
                PackageInfo pInfo = getPackageInfo(context, info.packageName);
                result.add(new QMApplication(
                        info.name, info.packageName, getPackageVersionName(pInfo), getPackageVersion(pInfo)));
                System.out.printf("AN APP: %s %s %s %s %s\n", info.name, info.packageName, info.dataDir, info.metaData, info.processName);

            }

        }

        Collections.sort(result, new Comparator<QMApplication>() {
            @Override
            public int compare(QMApplication o1, QMApplication o2) {
                int c = 0;
                if(o1!=null&&o2!=null && o1.packageName!=null&&o2.packageName!=null){
                    c  = o1.packageName.compareTo(o2.packageName);
                }
                return c;
            }
        });
        return result;
    }

    public static int getDBVersion(Context appContext, String dbName) {
        int v = 1;
        try {
            v = new SQLiteOpenHelper(appContext, dbName, null, 1) {
                @Override
                public void onCreate(SQLiteDatabase db) {

                }

                @Override
                public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

                }
            }.getWritableDatabase().getVersion();

            System.out.printf("the version is %s\n", v);

        } catch (Exception e) {
            String v2 = e.getMessage().replaceAll("(.*version +)|( +to.*)", "");
            System.err.printf("ops the version is %s\n", v2);
            try {
                v = Integer.parseInt(v2);
            } catch (NumberFormatException e1) {
                e1.printStackTrace();
            }

        }
        return v;
    }

    public static File[] getDatabasesFiles(String app) {
        String path = String.format(DATABASES_PATH, app);
        File dbFolder = new File(path);
        File[] files = dbFolder.listFiles();
        if (files == null) {
            files = new File[0];
        }
        return files;
    }

    public static List<QMDatabase> getDatabases(Context context, String app) {

        File[] files = getDatabasesFiles(app);

        List<QMDatabase> dbs;
        dbs = new ArrayList<QMDatabase>();
        Context fcontext = null;
        try {
            fcontext = getContext(context, app);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if(fcontext!=null) {
                for (File f : files) {
                    String dbName = f.getName();
                    System.out.println(f.getAbsolutePath());
                    if (dbName.contains("-journal")) continue;
                    dbs.add(new QMDatabase(app, dbName, getDBVersion(fcontext, dbName)));
                }
            }

        return dbs;
    }

    public static void printDBs(String app) {
        File[] dbs = getDatabasesFiles(app);


        for (File f : dbs) {
            System.out.println(f.getAbsolutePath());
            String dbName = f.getName();
            if (dbName.contains("-journal")) continue;


        }

    }

    public static QMDatabase getDatabase(Context context, String app, int dbIndex) {
        List<QMDatabase> dbs = getDatabases(context, app);
        QMDatabase db = null;
        if(!dbs.isEmpty()){
            db = dbs.get(dbIndex);
        }
        return db;
    }
}