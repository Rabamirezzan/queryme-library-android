package com.queryme.androidlib;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.provider.Settings.Secure;
import android.util.Base64;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.queryme.lib.models.*;
import com.queryme.lib.process.QueryMeCommands;
import com.queryme.lib.tools.Chronometer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class QueryMeServer {
    // public static final String EXEC_COMMAND = "[_EXEC_]";
    // public static final String QUERY_COMMAND = "[_QUERY_]";
    // public static final String SHARED_PREFERENCES_COMMAND =
    // "[_SHARED_PREFERENCES]";
    // public static final String DEVICE_INFO_COMMAND = "[_DEVICE_INFO_]";
    // public static final String DATABASE_INFO_COMMAND = "[_DATABASE_INFO_]";
    private static QueryMeServer instance;
    // "SELECT productId,code, name, Price.* FROM Product JOIN Price ON  Product.productId = Price.product GROUP BY Product.productId ORDER BY Product.name  limit 20"
    // private Class<? extends DBInfo> info;
    private Context context;

    private String dbApplication;
    private String dbName;
    private int dbVersion;

    private String systemDbName;
    private int systemDbVersion;

    {
        dbName = "";
        dbVersion = 0;
    }

    public static QueryMeServer getInstance(Context context) {
        if (instance == null) {
            instance = new QueryMeServer();
            instance.context = context;

        }

        return instance;
    }

    // protected void setDBInfo(Class<? extends DBInfo> info){
    // this.info = info;
    // }
    //
    // private SQLiteDatabase getDB(){
    // if(db==null ||!db.isOpen()){
    // System.out.println("new SQLiteDatabase");
    // DBHelper.setContext(context, info);
    //
    // db = DBHelper.getHelper().getWritableDatabase();
    // }
    //
    // return db;
    // }

    boolean appContext = true;

    private Context _getContext() throws NameNotFoundException {
        return _getContext(dbApplication);
    }

    private Context _getContext(String appPackage) throws NameNotFoundException {
        Context context = this.context;

        if (appPackage != null && !appPackage.trim().equals("") && appContext) {
            context = ApplicationListRetriever.initForeignContext(this.context, appPackage);
        } else {
            appContext = false;
            context = this.context;
        }
        return context;
    }

    protected SQLiteDatabase getDB() throws NameNotFoundException {
        Context context = _getContext();
        System.out.println("Context: " + context.getPackageName());
        SQLHelper.clearInstance();
        return SQLHelper.getInstance(context, dbName, dbVersion)
                .getWritableDatabase();
    }

    // protected SQLiteDatabase getDB(String appPackage) {
    //
    // return SQLHelper.getInstance(context, dbName, dbVersion)
    // .getWritableDatabase();
    // }

    protected void internalExec(String sql, String... args)
            throws NameNotFoundException {
        SQLiteDatabase db = getDB();
        db.beginTransaction();
        boolean exceptionThrown = false;
        SQLException exc = null;
        try {
            db.execSQL(sql, args);
        } catch (SQLException e) {
            exc = e;
            exceptionThrown = true;
            e.printStackTrace();
        } finally {
            db.setTransactionSuccessful();
            db.endTransaction();
            db.close();
        }

        if (exceptionThrown) {
            throw exc;
        }


    }

    protected QMResult internalQuery(String query, String... args)
            throws Exception {
        QMResult result = new QMResult();

        Exception exception = null;
        SQLiteDatabase db = getDB();
        try {

            db.beginTransaction();
            Cursor cursor = getDB().rawQuery(query, args);

            try {
                if (cursor.moveToFirst()) {
                    result.columns = cursor.getColumnNames();
                    String[] row = null;

                    do {
                        row = new String[cursor.getColumnCount()];
                        for (int i = 0; i < cursor.getColumnCount(); i++) {
                            try {
                                row[i] = cursor.getString(i);
                            } catch (Exception exc) {
                                byte[] blob = cursor.getBlob(i);
                                row[i] = Base64.encodeToString(blob,
                                        Base64.DEFAULT);// new String(blob);
                                System.out.println(row[i]);
                            }
                        }
                        result.rows.add(row);
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {

                e.printStackTrace();
                exception = e;
            } finally {
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            exception = e;

        } finally {
            db.setTransactionSuccessful();
            db.endTransaction();
            db.close();
            System.out.println("transaction end");
        }
        if (exception != null) {
            throw exception;
        }
        return result;
    }

    public void testDBConnection(Context context) {

    }

    private ServerSocket server;
    private boolean stop;

    public QMDevice getDeviceInfo(String host, int port, int timeout) {
        return execCommand(host, port, timeout, QMDevice.class,
                QueryMeCommands.DEVICE_INFO_COMMAND);
    }

    public <T> T execCommand(String host, int port, int timeout,
                             Class<T> resultClass, String command, String... params) {
        T result = null;
        Gson gson = getGson();
        String sparams = gson.toJson(params);
        try {
            // System.out.println("Iniciando Socket");
            Socket socket = new Socket();

            socket.connect(new InetSocketAddress(host, port), timeout);
            // System.out.println("Obteniendo printWriter");
            PrintWriter pw = new PrintWriter(socket.getOutputStream());

            pw.println(command);
            pw.flush();

            pw.println(sparams);
            pw.flush();

            // System.out.println("Enviando " + Arrays.asList(params));
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    socket.getInputStream()));

            String json = br.readLine();

            // System.out.println("obteniendo " + json);
            result = getGson().fromJson(json, resultClass);
            // System.out.println("result " + Arrays.asList(result.columns));

            pw.close();
            br.close();
        } catch (JsonSyntaxException e) {

            e.printStackTrace();
        } catch (UnknownHostException e) {

            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        }
        return result;
    }

    public void stop() {
        stop = true;
        try {
            server.close();
        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    public static Gson getGson() {
        return new GsonBuilder().serializeNulls().create();
    }

    private void responseExec(String[] params, BufferedReader in,
                              PrintWriter out) {
        System.out.println("Esperando exec");
        String query = params[0];
        System.out.println("exec " + query);
        QMResult result = new QMResult();
        Chronometer cn = new Chronometer();
        try {
            cn.start();
            internalExec(query, new String[]{});
            cn.stop();
            result.message = "Ok ";
        } catch (Exception e) {
            cn.stop();
            result.message = e.getMessage().concat(" ");

        } finally {
            result.message = result.message.concat(String.format(
                    "[ %d milliseconds]", cn.getTime()));
        }
        flushJson(result, out);
    }

    private void responseQuery(String[] params, BufferedReader in,
                               PrintWriter out) {
        System.out.println("Esperando query");
        String query = params[0];
        System.out.println("query " + query);
        QMResult result = new QMResult();
        Chronometer cn = new Chronometer();

        try {
            cn.start();
            result = internalQuery(query, new String[]{});
            cn.stop();
            result.message = "Ok ";
        } catch (Exception e) {
            cn.stop();
            result.message = e.getClass().toString().concat(": ")
                    .concat(e.getMessage()).concat(" ");

        } finally {
            result.message = result.message.concat(String.format(
                    "[ %d milliseconds]", cn.getTime()));
        }

        AppResponse_<QMResult> response = new AppResponse_<QMResult>(result, result.message);
        flushJson(response, out);
    }

    private void flushJson(Object result, PrintWriter out) {
        Gson gson = getGson();

        String json = gson.toJson(result);
        out.println(json);
        out.flush();
    }

    private void responseDeviceInfo(BufferedReader in,
                                    PrintWriter out) throws NameNotFoundException {
        QMDevice device = new QMDevice();
//		Context context = getContext(appPackage);
//		device.packageName = getPackageName(context);
//		device.packageVersion = getPackageVersion(context);
        device.deviceId = getDeviceId(context);
        device.device = Build.DEVICE;
        device.user = Build.USER;
        device.product = Build.PRODUCT;
        device.manufacter = Build.MANUFACTURER;
        device.brand = Build.BRAND;
        device.model = Build.MODEL;
        device.id = Build.ID;
        device.hardware = Build.HARDWARE;

        AppResponse_<QMDevice> response = new AppResponse_<QMDevice>(device, "Device info.");
        flushJson(response, out);
    }

    private List<QMApplication> getApplications() {
        List<QMApplication> list = ApplicationListRetriever.getApplications(context);
        return list;
    }

    private String getAppName(int index) {
        List<QMApplication> list = getApplications();
        String name = "";
        if (index >= 0 && index < list.size()) {
            name = list.get(index).packageName;
        }
        return name;
    }

    private void responseAvailableApplications(BufferedReader in, PrintWriter out) throws NameNotFoundException {
        List<QMApplication> apps = getApplications();
        AppResponse_<List<QMApplication>> response = new AppResponse_<List<QMApplication>>(apps, "Available applications. -select app 'n'");
        flushJson(response, out);
    }

    private void responseAvailableDatabases(String app, BufferedReader in, PrintWriter out) throws NameNotFoundException {
        List<QMDatabase> list = ApplicationListRetriever.getDatabases(context, app);
        AppResponse_<List<QMDatabase>> response = new AppResponse_<List<QMDatabase>>(list, "Available databases. -select db 'n'");
        flushJson(response, out);
    }

    private static String getDeviceId(Context context) {

        String android_id = Secure.getString(context.getContentResolver(),
                Secure.ANDROID_ID);
        // Toast.makeText(context, "devid:"+android_id, 1).show();
        return android_id;
    }

    private static String getPackageName(Context context) {
        return context.getPackageName();
    }


    protected static void getCommandAndParams(StringBuilder command,
                                              AtomicReference<String[]> params, BufferedReader br) {
        try {
            String c = br.readLine(); //First line is the command
            String sparams = br.readLine(); //Second line is the parameters
            command.delete(0, command.length()); //Deleting data of command StringBuilder
            command.append(c); // Adding command
            String[] aparams = getGson().fromJson(sparams, String[].class); //getting params from json.
            params.set(aparams);
        } catch (JsonSyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    protected static void execute(String command, String[] params,
                                  BufferedReader br, PrintWriter pw, QueryMeServer server)
            throws NameNotFoundException {
        System.out.println("Command to execute "+command);
        if (command.equals(QueryMeCommands.QUERY_COMMAND)) {
            server.responseQuery(params, br, pw);
        } else if (command.equals(QueryMeCommands.EXEC_COMMAND)) {
            server.responseExec(params, br, pw);
        } else if (command.equals(QueryMeCommands.DEVICE_INFO_COMMAND)) {

            server.responseDeviceInfo(br, pw);
        } else if (command.equals(QueryMeCommands.APPLICATIONS_QUERY_COMMAND)) {

            server.responseAvailableApplications(br, pw);
        } else if (command.equals(QueryMeCommands.DATABASES_QUERY_COMMAND)) {
            System.out.println("Attending "+QueryMeCommands.DATABASES_QUERY_COMMAND);
            int index = Integer.parseInt(params[0]);
            String app = server.getAppName(index);
            System.out.println("Retrieving for "+app);
            server.responseAvailableDatabases(app, br, pw);
        } else if (command.equals(QueryMeCommands.DATABASE_SELECTION_COMMAND)) {
            int appIndex = Integer.parseInt(params[0]);
            int dbIndex = Integer.parseInt(params[1]);
            String app = server.getAppName(appIndex);
            QMDatabase database = server.getDatabase(app, dbIndex);
            System.out.println("DB: "+database);
            if (database != null) {
                server.dbApplication = app;
                server.dbName = database.name;
                server.dbVersion = database.version;
                System.out.printf("Database selected, app: %s, db: %s [%s] \n", app, database.name, database.version);
            }
            server.responseSelectedDatabase(database, br, pw);
        } else if (command.equals(QueryMeCommands.DATABASE_INFO_COMMAND)) {
            //server.setDBData(params);
        } else if (command.equals(QueryMeCommands.SHARED_PREFERENCES_COMMAND)) {
            server.getSharedPreferences(params, br, pw);
        }
    }

    private void responseSelectedDatabase(QMDatabase database, BufferedReader in, PrintWriter out) {
        AppResponse_<QMDatabase> response = new AppResponse_<QMDatabase>(database, "Database selected.");
        flushJson(response, out);
    }

    private QMDatabase getDatabase(String app, int dbIndex) {
        return ApplicationListRetriever.getDatabase(context, app, dbIndex);
    }

    private void getSharedPreferences(String[] params, BufferedReader br,
                                      PrintWriter pw) {
        QMResult result = new QMResult();
        Chronometer cn = new Chronometer();
        cn.start();
        try {
            if (params.length > 0) {
                System.out.println("Executing ".concat(params[0]));
                Object rs = DyOb.execute(_getContext(), params[0]);
                result = new QMResult();
                result.rawData = rs;
                result.message = "OK! ";
            } else {
                result.message = "NO params ";
            }
        } catch (Exception e) {
            result.message = e.getMessage();
            e.printStackTrace();
        }
        cn.stop();

        String m = String.format("[ %d milliseconds]", cn.getTime());
        result.message = result.message.concat(m);
        AppResponse_<QMResult> response = new AppResponse_<QMResult>(result, result.message);
        flushJson(response, pw);

    }

    public boolean isConnected() {
        boolean connected = false;
        connected = server != null && !server.isClosed();
        return connected;

    }

    public void connect(int port, final String dbName, final int dbVersion) {
        try {

            server = new ServerSocket(port);
            this.systemDbName = this.dbName = dbName;
            this.systemDbVersion = this.dbVersion = dbVersion;

            Runnable runn = new Runnable() {

                @Override
                public void run() {
                    stop = false;
                    do {
                        try {
                            System.out.println("esperando socket");
                            Socket socket = server.accept();

                            if (!stop) {
                                BufferedReader br = new BufferedReader(
                                        new InputStreamReader(
                                                socket.getInputStream()));
                                PrintWriter pw = new PrintWriter(
                                        socket.getOutputStream());

                                StringBuilder command = new StringBuilder();
                                AtomicReference<String[]> params = new AtomicReference<String[]>();
                                getCommandAndParams(command, params, br);
                                System.out.println(command + " "
                                        + Arrays.asList(params.get()));
                                try {
                                    execute(command.toString(), params.get(),
                                            br, pw, QueryMeServer.this);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    QMResult result = new QMResult();
                                    result.message = e.getMessage();
                                    flushJson(result, pw);
                                }
                                br.close();
                                pw.close();
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    } while (!stop);
                }

            };

            Thread thread = new Thread(runn);
            thread.start();
        } catch (IOException e) {

            // e.printStackTrace();
        }
    }

    private void setDBData(String[] params) throws NameNotFoundException {

        if (params.length >= 2)
            if (validateDBInfo(params[0], params[1])) {

                this.dbName = params[0];
                this.dbVersion = Integer.valueOf(params[1]);
                if (params.length == 3) {
                    appContext = true;
                    this.dbApplication = params[2];
                }
            } else {
                this.dbName = systemDbName;
                this.dbVersion = systemDbVersion;
            }
        getDB();
    }

    private boolean validateDBInfo(String dbName, String dbVersion) {
        boolean result = true;
        try {
            // result = !appPackage.trim().equals("");
            result = dbName != null && !dbName.trim().equals("");
            Integer.valueOf(dbVersion);

        } catch (Exception ex) {
            result = false;
        }

        return result;
    }


}
