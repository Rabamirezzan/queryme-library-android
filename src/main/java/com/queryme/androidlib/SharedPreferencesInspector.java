package com.queryme.androidlib;

import android.content.Context;
import android.content.SharedPreferences;

import java.io.Serializable;
import java.util.Set;

public class SharedPreferencesInspector {
	public static Set<String> getSharedPreferencesKeys(Context context,
			String name, int mode) {
		SharedPreferences sp = context.getSharedPreferences(name, mode);
		Set<String> keys = sp.getAll().keySet();
		return keys;
	}

	public static Serializable getSharedPreferences(Context context, String name,
			int mode, String key) {
		SharedPreferences sp = context.getSharedPreferences(name, mode);
		System.out.println(context.getPackageName()+" "+name+" "+mode+" "+key);
		Object value = sp.getString(key, "");
		return (Serializable) value;
	}
	
	public static Object setSharedPreferences(Context context, String name,
			int mode, String key, Object value) {
		SharedPreferences sp = context.getSharedPreferences(name, mode);
//		if (value instanceof ){
//			sp.edit().p
//		}
		return value;
	}
}
